﻿#include <iostream>

void welcome()
{
    std::cout << "Welcome into developing!\n";
}

void printf(int a, int b, int c)
{
    std::cout << "first = " << a << "\n";
    std::cout << "second = " << b << "\n";
    std::cout << "last = " << c << "\n";
}

int main()
{
    int x = 2;
    int y = 1;
    int z = x*2 + y;

    std::cout << "Hello World!\n";

    welcome();

    printf(x,y,z);
}
